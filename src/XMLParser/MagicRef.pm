package XMLParser::MagicRef;

use 5.010_001;
use strict;
use warnings;
use Carp;

=head1 SUBROUTINES

All subroutine names begin with C<mref_>, so that they can't interfere with
the name of a method from the underlying object (once the reference is
resolved).

=over

=item mref_new( ID )

Returns a new, unresolved XMLParser::MagicRef object holding the identifier
C<ID>.

=cut

sub mref_new { bless { value => $_[1], resolved => 0 } => $_[0] }


=item mref_resolved

Returns 1 if the reference has been resolved, 0 otherwise.

=cut

sub mref_resolved { $_[0]->{resolved} }


=item mref_value

Returns the value currently associated to the reference. Should not be used
without a previous call to C<mref_resolved>!

=cut

sub mref_value { $_[0]->{value} }


=item mref_resolve( VALUE )

Replace the reference's C<ID> with the given C<VALUE>, and marks the reference
as I<resolved>.

=cut

sub mref_resolve {
    my ($self, $value) = @_;
    croak "MagicRef already resolved!" if $self->mref_resolved;
    $self->{value} = $value;
    $self->{resolved} = 1;
    return $self;
}


=item (every other method)

XMLParser::MagicRef objects are I<AUTOLOAD>ed: every method not listed before
is passed "as is" to the underlying object (but only if the reference has been
resolved).

=cut

sub AUTOLOAD {
    my $self = shift;
    croak "MagicRef not yet resolved!" unless $self->mref_resolved;
    our $AUTOLOAD;
    $AUTOLOAD =~ s/^XMLParser::MagicRef::/ ref($self->{value}) . '::' /e;
    unshift @_, $self->{value};
    goto &$AUTOLOAD;
}

1;
