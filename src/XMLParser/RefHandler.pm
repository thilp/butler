package XMLParser::RefHandler;

use 5.010_001;
use strict;
use warnings;

use Carp;

use XMLParser::MagicRef;

=head1 SUBROUTINES

=over

=item new

Returns a new XMLParser::RefHandler instance. Each instance manage its own
separate set of IDs/references.

=cut

sub new {
    return bless { orig => [], antimap => {}, dest => {} } => $_[0];
}

=item ask_for( ID1 [ , ... ] )

Returns references associated with the given IDs. Depending on the state of
the program, each of these references can already be resolved, or not.

=cut

sub ask_for {
    my $self = shift;

    ref $self
      or croak 'ask_for is an instance method (called as a class method)';

    # Resolve already known (= in $self->{antimap}) IDs
    my @refs =
      map {
        exists $self->{antimap}{$_}
          ? $self->{orig}[ $self->{antimap}{$_} ]
          : XMLParser::MagicRef->mref_new($_)
      } @_;

    # Resolve already known (= in $self->{dest}) refs
    $_->mref_resolve( $self->{dest}{ $_->mref_value } )
      for grep { exists $self->{dest}{ $_->mref_value } } @refs;

    # Update $self->{antimap}
    my $old_idx = 1 + $#{ $self->{orig} };
    @{ $self->{antimap} }{ map { $_->mref_value } @refs } =
      $old_idx .. ( $old_idx + @refs - 1 );

    # Add refs to $self->{orig}
    push @{ $self->{orig} }, @refs;

    return wantarray ? @refs : ( @refs < 2 ? shift @refs : @refs );
}

=item offer( ID1 => OBJ1 [ , ... ] )

Returns nothing, but associates C<OBJ1> to C<ID1> (and so on), effectively
resolving references. If a given ID doesn't exist yet, the associated
reference will be resolved as soon as the ID will be defined (using
C<ask_for>).

If an ID, say I<origin>, is already linked to a value I<A> and you try to
re-link I<origin> to another value I<B> with C<offer>, I<origin> will finally
be associated to I<B> and I<A> will be lost for the RefHandler.

=cut

sub offer {
    my $self  = shift;
    my %pairs = @_;

    ref $self
      or croak 'offer is an instance method (called as a class method)';

    while ( my ( $k, $v ) = each %pairs ) {
        $self->{dest}{$k} = $v;
        if ( exists $self->{antimap}{$k} ) {
            $self->{orig}[ $self->{antimap}{$k} ]->mref_resolve($v)
        }
    }
    return;
}

1;
