package XMLParser::XMLObject;

use 5.010_001;
use strict;
use warnings;
use utf8;
use open IO => 'utf8';
use open ':std';
use autodie;

use Moose;
use Moose::Util::TypeConstraints;

use XML::LibXML 2.00;
use List::MoreUtils qw( zip );

use lib '..';

use Roles::Logging;
use XMLParser::RefHandler;

my $XMLPARSER;     # XML::LibXML::Parser singleton instance
my %DISPATCH;      # Dispatch a node type to the appropriate handler
my $REFHANDLER;    # Holds all resolved and still unresolved magic references

INIT {
    $REFHANDLER = XMLParser::RefHandler->new;
    $XMLPARSER  = XML::LibXML->new(
        line_numbers        => 1,
        pedantic_parser     => 1,
        no_blanks           => 1,
        complete_attributes => 1,
        validation          => 1,
    );
}

# FILE #######################
# The XML source: can be a filehandle, a filename or a XML string.

subtype 'XMLParser::FileHandle', as 'FileHandle';
coerce 'XMLParser::FileHandle',
    from 'Str' => via {
        my $fh;
        if (-e) { open $fh, '<', $_ }        # it's a path
        else    { open $fh, '<', \$_ }    # xml contained in the string
        $fh;
    };

has file => (
    is       => 'ro',
    isa      => 'XMLParser::FileHandle',
    required => 1,
);

# LOGGERS ####################
# These objects are listeners that do Roles::Logging. They will all be
# called when remarquable events happen, and can be added and removed
# dynamically.

has loggers => (
    is      => 'rw',
    isa     => 'HashRef[Roles::Logging]',
    default => sub { {} },
    traits  => ['Hash'],
    handles => {
        list_loggers      => 'values',
        add_loggers       => 'set',
        remove_loggers    => 'delete',
        clear_loggers     => 'clear',
    },
);

# XML ########################
# The DOM object produced from parsing the 'file' attribute's content.

has xml => (
    is      => 'bare',
    isa     => 'XML::LibXML::Document',
    lazy    => 1,
    builder => '_build_xml',
);

sub _build_xml {
    my $self = shift;

    $self->info("parsing XML ...");
    my $xml = eval { $XMLPARSER->load_xml( IO => $self->file ) };
    if ($@) {
        $self->fatal("XML::LibXML::Parser can't load this XML: $@");
        die;
    }
    $xml = $xml->documentElement;

    $self->debug("removing XML comments ...");
    my $comments = $xml->findnodes('descendant-or-self::comment()');
    $comments->foreach( sub { $_->unbindNode } );

    return $xml;
}


# AUTOLOAD allows to call Roles::Logging method directly on all listeners
# from $self.
sub AUTOLOAD {
    my $self = shift;
    our $AUTOLOAD;

    if ( grep { $AUTOLOAD eq $_ }
        Roles::Logging->meta->get_required_method_list )
    {
        foreach my $logger ( $self->list_loggers ) {
            my @copy = @_;
            $logger->$AUTOLOAD(@copy);
        }
    }
    else {
        unshift @_ => $self;
        goto &$AUTOLOAD;
    }
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
