package Roles::Subnamed;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose::Role;

has subname => (
    is        => 'rw',
    isa       => 'Str',
    predicate => 'has_subname',
);

no Moose;
1;
