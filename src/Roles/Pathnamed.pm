package Roles::Pathnamed;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose::Role;

has path => (
    is => 'rw',
    isa => 'Objects::Path',
    predicate => 'has_path',
);

no Moose;
1;
