package Roles::Limited;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose::Role;

has period => (
    is => 'rw',
    isa => 'Objects::Period',
    predicate => 'has_period',
);

1;
