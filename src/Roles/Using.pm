package Roles::Using;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose::Role;
use XMLParser::MagicRef;

has using => (
    is      => 'ro',
    isa     => 'ArrayRef[XMLParser::MagicRef]',
    traits  => ['Array'],
    handles => {
        add_languages   => 'push',
        languages       => 'elements',
        count_languages => 'count',
    },
);

no Moose;
1;
