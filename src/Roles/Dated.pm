package Roles::Dated;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose::Role;

has date => (
    is => 'rw',
    isa => 'Objects::Date',
    predicate => 'has_date',
);

no Moose;
1;
