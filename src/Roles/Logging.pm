package Roles::Logging;

use 5.010_001;
use strict;
use warnings;

use Moose::Role;

# All these methods will be fed with a single string.
# No return value is expected.

requires 'debug';      # debug-level message
requires 'info';       # informational message
requires 'notice';     # normal, but significant, condition
requires 'warning';    # warning conditions
requires 'error';      # error conditions
requires 'critic';     # critical conditions
requires 'alert';      # action must be taken immediately
requires 'fatal';      # system is unusable

# (Inspired by Sys::Syslog's levels.)

no Moose;
1;
