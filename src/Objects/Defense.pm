package Objects::Defense;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

extends 'Objects::Document';
with 'Roles::Dated';

has 'file' => (
    is => 'ro',
    isa => 'Objects::File',
    required => 1,
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
