package Objects::File;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 2;

use Moose;

extends 'Objects::Document';
with 'Roles::Pathnamed', 'Roles::Limited';

has descr => ( is => 'rw', isa => 'Str' );
has mandatory => ( is => 'rw', isa => 'Bool', default => 0 );

no Moose;
__PACKAGE__->meta->make_immutable;
1;
