package Objects::Path;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use Cwd;
use File::Spec;

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;
    if ( @_ == 1 and !ref $_[0] ) {
        return $class->$orig( verbatim => $_[0] );
    }
    else { return $class->$orig(@_) }
};

has verbatim => (
    is       => 'bare',
    isa      => 'Str',
    required => 1,
    reader   => 'get_canonical',
);

has base => (
    is      => 'bare',
    isa     => 'Str',
    reader => 'get_base',
    writer => 'set_base',
    lazy    => 1,
    default => sub { Cwd::cwd },
);

around 'set_base' => sub {
    my ( $orig, $self ) = @_;
    return $self->$orig( Cwd::realpath($_[0]) );
};

has real => (
    is      => 'bare',
    isa     => 'Str',
    reader  => 'get_absolute',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return _compute_real( $self->get_canonical, $self->get_base );
    },
);

sub _compute_real {
    my ( $verbatim, $base ) = @_;

    return Cwd::realpath(
        File::Spec->file_name_is_absolute($verbatim)
        ? $verbatim
        : File::Spec->catfile( $base, $verbatim )
    );
}

sub get_relative {
    return File::Spec->abs2rel( $_[0]->get_absolute, $_[0]->get_base );
}

sub ensure {
    my ( $self, $code ) = @_;
    local $_ = $self->get_absolute;
    return &$code;
}

sub standard_for {
    my ($class, $doctype) = @_;
    confess "standard_for is a static method: can't call it on a ref!" if ref $class;
    if ($doctype eq 'subject') { Objects::Path->new( 'subject' ) }
    elsif ($doctype eq 'tutorial') { Objects::Path->new( 'tutorials' ) }
    elsif ($doctype eq 'slides') { Objects::Path->new( 'slides' ) }
    elsif ($doctype eq 'other') { confess "no default path for doctype 'other'" }
    elsif ($doctype eq 'defense') { Objects::Path->new('defenses') }
    elsif ($doctype eq 'examsubject') { Objects::Path->new( 'subject' ) }
    else { confess "unknown document type '$doctype'" }
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
