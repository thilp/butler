package Objects::Project;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use List::Util qw(first);
use Objects::Types;

with 'Roles::Limited';

has name => ( is => 'ro', isa => 'Str', required => 1 );

has managers => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Manager]',
    default => sub { [] },
    traits  => ['Array'],
    handles => {
        add_managers   => 'push',
        list_managers  => 'elements',
        count_managers => 'count',
    },
);

has news => ( is => 'rw', isa => 'Objects::News' );

has grouping => (
    is        => 'rw',
    isa       => 'Objects::Grouping',
    predicate => 'has_grouping'
);

has languages => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Language]',
    default => sub { [] },
    traits  => ['Array'],
    handles => {
        add_languages   => 'push',
        list_languages  => 'elements',
        count_languages => 'count',
    },
);

has targets => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Target]',
    default => sub { [] },
    traits  => ['Array'],
    handles => {
        add_targets   => 'push',
        list_targets  => 'elements',
        count_targets => 'count',
    },
);

has repositories => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Repository]',
    default => sub { [] },
    traits  => ['Array'],
    handles => {
        add_repositories   => 'push',
        list_repositories  => 'elements',
        count_repositories => 'count',
    },
);

has written_in => (is => 'rw', isa => 'Butler::SpokenLanguage');

has documents => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Document]',
    default => sub { [] },
    traits  => ['Array'],
    handles => {
        add_documents   => 'push',
        list_documents  => 'elements',
        count_documents => 'count',
    },
);

sub get_document {
    my ($self, $name) = @_;
    return first { $_->name eq $name } $self->list_documents;
}

has default_written_in => (
    is => 'rw',
    isa => 'Butler::SpokenLanguage',
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
