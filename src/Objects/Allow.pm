package Objects::Allow;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use Moose::Util::TypeConstraints;

has from_lib => ( is => 'rw', isa => 'Str' );
has content  => ( is => 'ro', isa => 'Str', required => 1 );
has manpage  => ( is => 'rw', isa => 'Str' );
has url      => ( is => 'rw', isa => 'Str' );

has kind => ( # 'type' was already taken
    is       => 'ro',
    isa      => enum( [qw( function library package other )] ),
    required => 1,
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
