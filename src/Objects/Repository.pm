package Objects::Repository;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use Objects::Types;

has kind => ( is => 'rw', isa => 'Str', default => 'Git' );

has quota => ( is => 'rw', isa => 'Butler::PositiveInt', default => 20 );

has url => ( is => 'rw', isa => 'Str' );

no Moose;
__PACKAGE__->meta->make_immutable;
1;
