package Objects::Dir;

use 5.010_001;
use strict;
use warnings;

use Moose;

extends 'Objects::File';

has 'tree' => (
    is      => 'rw',
    isa     => 'ArrayRef[Objects::File]',
    traits  => ['Array'],
    handles => {
        add   => 'push',
        list  => 'elements',
        count => 'count',
    },
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
