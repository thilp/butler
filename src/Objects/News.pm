package Objects::News;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

has newsgroup => ( is => 'ro', isa => 'Str', required => 1 );
has tag       => ( is => 'ro', isa => 'Str', required => 1 );

no Moose;
__PACKAGE__->meta->make_immutable;
1;
