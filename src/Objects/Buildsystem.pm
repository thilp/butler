package Objects::Buildsystem;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

has command => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has buildrules => (
    is      => 'ro',
    isa     => 'ArrayRef[Objects::Buildrule]',
    traits  => ['Array'],
    handles => {
        add_rules   => 'push',
        list_rules  => 'elements',
        count_rules => 'count',
    },
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
