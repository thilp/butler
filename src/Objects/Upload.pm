package Objects::Upload;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

has tree => (
    is => 'rw',
    isa => 'Objects::TopLevelDir'
);

has format => (
    is => 'rw',
    isa => 'Str',
    default => 'tar.bz2',
);

has url => (
    is => 'rw',
    isa => 'Str',
    required => 1,
);

has identifier => (
    is => 'rw',
    isa => 'Str',
);

has quota => (
    is => 'rw',
    isa => 'Butler::PositiveInt',
    default => 10,
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
