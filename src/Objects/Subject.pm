package Objects::Subject;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

extends 'Objects::Document';
with 'Roles::Subnamed', 'Roles::Limited', 'Roles::Pathnamed', 'Roles::Using';

has submissions => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Submission]',
    traits  => ['Array'],
    handles => {
        add_submissions   => 'push',
        submissions       => 'elements',
        count_submissions => 'count',
    },
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
