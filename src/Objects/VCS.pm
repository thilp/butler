package Objects::VCS;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

has tree => (
    is => 'rw',
    isa => 'Objects::TopLevelDir',
);

has repo_id => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

has token => (
    is => 'rw',
    isa => 'Str',
);

has tag => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
