package Objects::Tutorial;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

extends 'Objects::Document';
with 'Roles::Subnamed', 'Roles::Limited', 'Roles::Pathnamed', 'Roles::Using';

1;
