package Objects::Manager;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use lib '..';
use Moose;
use Objects::Types;

has firstname => ( is => 'ro', isa => 'Str',           required => 1 );
has lastname  => ( is => 'ro', isa => 'Str',           required => 1 );
has login     => ( is => 'ro', isa => 'Butler::Login', required => 1 );

no Moose;
__PACKAGE__->meta->make_immutable;
1;
