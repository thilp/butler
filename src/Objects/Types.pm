package Objects::Types;

use 5.010_001;
use strict;
use warnings;
use feature 'state';

our $VERSION = 1;

use Moose;
use Moose::Util::TypeConstraints;
use File::Spec;

############

subtype 'Butler::VersionString'
    => as 'Str'
    => where { / ^ [vV]? \h* [0-9]+ (?: \. [0-9]+ ){0,2} $ /x };

coerce 'Butler::VersionString'
    => from 'Str'
    => via { $_ = s/ ^ [vV]? \h* //x; $_ };

############

subtype 'Butler::PositiveInt'
    => as 'Int'
    => where { $_ >= 0 };

############

enum 'Butler::SpokenLanguage' => [ 'en', 'fr' ];

############

subtype 'Butler::Login'
    => as 'Str'
    => where { /^[a-z0-9][a-z0-9-]{1,5}_[a-z0-9-]$/ };

############

enum 'Butler::DocumentType' => [ qw(
    subject
    examsubject
    tutorial
    slides
    other
    defense
)];

############

no Moose;
__PACKAGE__->meta->make_immutable;
1;
