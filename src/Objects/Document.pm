package Objects::Document;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use Objects::Types;

has 'name' => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

has 'written_in' => (
    is => 'rw',
    isa => 'Butler::SpokenLanguage',
    default => 'en',
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
