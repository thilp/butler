package Objects::ExamSubject;

use 5.010_001;
use strict;
use warnings;
use Carp;

our $VERSION = 1;

use Moose;

extends 'Objects::Document';
with 'Roles::Subnamed', 'Roles::Pathnamed', 'Roles::Using';

no Moose;
__PACKAGE__->meta->make_immutable;
1;
