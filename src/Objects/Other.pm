package Objects::Other;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

extends 'Objects::Document';
with 'Roles::Subnamed', 'Roles::Dated', 'Roles::Pathnamed';

no Moose;
__PACKAGE__->meta->make_immutable;
1;
