package Objects::Grouping;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use Objects::Types;

with 'Roles::Limited';

has max_groups  => ( is => 'rw', isa => 'Butler::PositiveInt' );
has max_members => ( is => 'rw', isa => 'Butler::PositiveInt', default  => 1 );

no Moose;
__PACKAGE__->meta->make_immutable;
1;
