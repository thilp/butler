package Objects::Exam;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

with 'Roles::Limited';

has 'name' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has managers => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Manager]',
    default => sub { [] },
    traits  => ['Array'],
    handles => {
        add_managers   => 'push',
        list_managers  => 'elements',
        count_managers => 'count',
    },
);

has languages => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Language]',
    default => sub { [] },
    traits  => ['Array'],
    handles => {
        add_languages   => 'push',
        list_languages  => 'elements',
        count_languages => 'count',
    },
);

has targets => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::Target]',
    default => sub { [] },
    traits  => ['Array'],
    handles => {
        add_targets   => 'push',
        list_targets  => 'elements',
        count_targets => 'count',
    },
);

has 'subject' => (
    is  => 'rw',
    isa => 'Objects::ExamSubject',
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
