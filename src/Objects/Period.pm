package Objects::Period;

use 5.010_001;
use strict;
use warnings;
use Carp;

our $VERSION = 2;

use Moose;

has [ 'begin', 'end' ] => (is => 'rw', isa => 'Objects::Date', required => 1);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
