package Objects::Link;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

extends 'Objects::Document';
with 'Roles::Limited';

has url => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

has descr => (
    is => 'rw',
    isa => 'Str',
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
