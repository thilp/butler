package Objects::Target;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use Moose::Util::TypeConstraints;
use Objects::Types;

has name => (is => 'ro', isa => 'Str', required => 1);

has kind => (
    is => 'rw',
    isa => enum([ 'compiler', 'interpreter' ]),
    default => 'compiler',
);

has version => (is => 'rw', isa => 'Butler::VersionString', coerce => 1);

has [ 'command', 'options' ] => (is => 'rw', isa => 'Str');

no Moose;
__PACKAGE__->meta->make_immutable;
1;
