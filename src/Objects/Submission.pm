package Objects::Submission;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use Moose::Util::TypeConstraints;
use Objects::Upload;
use Objects::VCS;

with 'Roles::Limited';

has name => ( is => 'ro', isa => 'Str', required => 1 );

has kind => (
    is      => 'rw',
    isa     => enum( [qw( classic oneshot presence )] ),
    default => 'classic',
);

has allow => (
    is      => 'rw',
    isa     => 'ArrayRef[Objects::Allow]',
    traits  => ['Array'],
    handles => {
        add_allow   => 'push',
        list_allow  => 'elements',
        count_allow => 'count',
    },
);

has buildsystem => (
    is => 'rw',
    isa => 'Objects::Buildsystem',
);

has method => (
    is => 'rw',
    isa => 'Objects::Upload | Objects::VCS',
    required => 1,
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
