package Objects::TopLevelDir;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

has _list => (
    is      => 'bare',
    isa     => 'ArrayRef[Objects::File]',
    traits  => ['Array'],
    default => sub { [] },
    handles => {
        add   => 'push',
        shift => 'shift',
        list  => 'elements',
        count => 'count',
    },
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
