package Objects::Language;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use XMLParser::MagicRef;

has name => ( is => 'ro', isa => 'Str', required => 1 );

has targets => (
    is      => 'ro',
    isa     => 'HashRef[XMLParser::MagicRef]',
    traits  => ['Hash'],
    default => sub { {} },
    handles => {
        get_targets   => 'get',
        add_targets   => 'set',
        list_targets  => 'keys',
        count_targets => 'count',
      }
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
