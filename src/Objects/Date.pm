package Objects::Date;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;
use Moose::Util::TypeConstraints;
use DateTime;
use DateTime::Format::ISO8601 0.08;

subtype 'DateTime' => as class_type('::DateTime');

coerce 'DateTime'
    => from 'Str'
    => via { DateTime::Format::ISO8601->parse_datetime($_) };

has 'dt' => (
    is       => 'ro',
    isa      => 'DateTime',
    required => 1,
    coerce   => 1,
    handles  => qr/^.*$/,
);

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;
    if ( @_ == 1 ) { return $class->$orig( dt => $_[0] ) }
    else           { return $class->$orig(@_) }
};

no Moose;
__PACKAGE__->meta->make_immutable;
1;
