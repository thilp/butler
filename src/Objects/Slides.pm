package Objects::Slides;

use 5.010_001;
use strict;
use warnings;

our $VERSION = 1;

use Moose;

extends 'Objects::Document';
with 'Roles::Subnamed', 'Roles::Pathnamed', 'Roles::Dated';

no Moose;
__PACKAGE__->meta->make_immutable;
1;
