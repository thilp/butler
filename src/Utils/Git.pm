package Utils::Git;

use 5.010_001;
use strict;
use warnings;
use utf8;
use open IO => 'utf8';
use open ':std';

our $VERSION = 1;

# All Git operations of this module are performed from the HEAD from where the
# script is executed.

use lib '..';    # debug

use Carp;
use Text::ParseWords 'quotewords';

use Utils::Run qw( can_run run );
use Utils::Say qw( warning );

BEGIN { can_run('git') or alert('git is not installed!') }

sub git ($;$) {    ## no critic ProhibitSubroutinePrototypes
    my ( $cmd, $args ) = @_;
    return unless $cmd;

    if ( defined $args ) {
        if ( ref $args ne 'ARRAY' ) {
            ref $args eq ''
              or croak 'git arguments should be given as string or arrayref';
            $args = [ quotewords(qr/\s+/, 0, $args) ];
        }
    }
    else { $args = [''] }

    return run( command => [ 'git', '--no-pager', $cmd, @$args ] );
}

# Facility above git() that inserts '--porcelain' in its second argument.
sub git_p ($;$) {    ## no critic ProhibitSubroutinePrototypes
    my ( $cmd, $args ) = @_;
    for ( ref $args ) {
        unshift @$args, '--porcelain' if $_ eq 'ARRAY';
        $args = '--porcelain ' . $args if $_ eq '';
    }
    git( $cmd => $args );
}

# Return the (chronologically) last tag on the current commit path that
# respects the given globbing expression.
# Just a wrapper on `git-describe(1)`.
# Return nothing if no tag is found.
sub _last_tag {
    my $glob = shift;
    not( ref $glob eq 'Regexp' ) or croak 'expected a string (shell globbing)';

    my $tag = eval {
        git( 'describe', qq{HEAD --tags --match '$glob'} );
    };
    croak $@ if $@;

    $tag =~ s/ - \d+ - g [a-f0-9]+ $ //x;
    chomp $tag;
    return $tag;
}

sub last_tag {
    my %arg = @_;

    my @doctypes = qw( subject tutorial slides other defense examsubject );
    my @optional_tag_parts = qw( version id path year );
    {
        exists $arg{doctype} && $arg{doctype} ne ''
          or croak 'doctype argument is mandatory';
        grep { $arg{doctype} eq $_ } @doctypes
          or croak 'unknown document type (expecting '
          . join( '/' => @doctypes )
          . '): ', $arg{doctype};
    }
    if ( exists $arg{version} ) {
          $arg{version} =~ /^[0-9]+$/
            or croak 'unknown version format (expecting [0-9]+): ',
            $arg{version};
    }
    if ( exists $arg{id} ) {
          $arg{id} =~ /^[a-z0-9_-]+$/
            or croak 'unknown id format (expecting [a-z0-9_-]+): ', $arg{id};
    }
    if ( exists $arg{path} ) {
          defined $arg{path} or croak "path can't be undef";
          $arg{path} !~ /,/
            or croak "path can't contain a comma (,): ", $arg{path};
    }
    if ( exists $arg{year} ) {
          $arg{year} =~ /^[0-9]+$/
            or croak 'unknown year format (expecting [0-9]+): ', $arg{year};
    }

    my $sep = q{,};
    my $qsep = quotemeta $sep;

    my $glob = $arg{doctype};
    $glob .= $sep . ($arg{$_} // '*') for @optional_tag_parts;
    $glob =~ s/ (?: $qsep \* )+ $ /*/x;

    my $tag = eval { _last_tag( $glob ) };
    if ($@) { warning("no corresponding tag found: $@"); goto RETURN }
    elsif (not defined $tag) { croak "no corresponding tag found" }

    $tag =~ / \Q$arg{doctype}\E
        (?: $qsep (?<version> [0-9]* )
            (?: $qsep [a-z0-9_-]* # intranet id
                (?: $qsep (?<path> [^$qsep]* )
                    (?: $qsep (?<year> [0-9]* )
                    )?
                )?
            )?
        )?
    /x;

    RETURN: return wantarray
      ? ( $arg{doctype}, $+{version} // 1, $+{path}, $+{year} )
      : {
        doctype => $arg{doctype},
        version => $+{version} // 1,
        path    => $+{path},
        year    => $+{year}
      };
}

1;
