package Utils::Run;

use 5.010_001;
use strict;
use warnings;
use utf8;
use open IO => 'utf8';
use open ':std';

our $VERSION = 1;

use lib '..';    # debug

use Carp 'croak';
use IPC::Cmd;

BEGIN {
    use parent 'Exporter';
    our @EXPORT_OK = qw( can_run run );
}

BEGIN { *can_run = *IPC::Cmd::can_run }

sub run {
    my %h = @_;

    my $buffer = '';
    $h{buffer} = \$buffer;

    my ( $ok, $err ) = IPC::Cmd::run(%h);

    chomp($err) if defined $err;
    unless ($ok) {
        if ( ref $h{command} eq 'ARRAY' ) {
            croak external_call_failed($err);
        }
        else { croak external_call_failed($err) }
    }

    return $buffer // 1;
}

sub external_call_failed {
    'external call failed: ' . $_[0] . ( @_ > 1 ? " ($_[1])" : '' ) . "\n";
}

1;
