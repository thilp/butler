package Utils::Say;

use 5.010_001;
use strict;
use warnings;
use autodie;
use open IO => ':utf8';
use open ':std';
use utf8;

BEGIN {
    use parent 'Exporter';
    our @EXPORT_OK = qw( info success warning alert terminate wait_enter
      interact process process_if );
}

use Carp;
use Term::ANSIColor qw(colorstrip :constants);
use Text::Tabs 'expand';
use List::Util qw(reduce);

use constant WIDTH => 80;

use constant SAYING => RESET . ' ' . BOLD . '> ' . RESET;
use constant {
    INFO_PROMPT      => BLUE . 'vvv' . SAYING,
    TERMINATE_PROMPT => BOLD . BLUE . '###' . SAYING,
    SUCCESS_PROMPT   => GREEN . '***' . SAYING,
    WARNING_PROMPT   => YELLOW . '/\/' . SAYING,
    ALERT_PROMPT     => BOLD . RED . '!!!' . SAYING,
    INTERACT_PROMPT  => BOLD . CYAN . '???' . SAYING,
};
use constant {
    INDENT_BEGIN    => "\N{U+23A1} ",
    INDENT_GUIDE    => "\N{U+23A2}",
    INDENT_COMEBACK => "\N{U+23A3} ",
};

my ($WARNING_COUNT, $ERROR_COUNT, $INDENT);
local ($a, $b);

# For wrap()
my ($RE_COLOR, @WRAP_BUFFER, @WRAP_STACK, $WRAP_COLUMNS, $WRAP_SUBSEQ_TAB, $WRAP_STACKSIZE);

BEGIN {
    $WARNING_COUNT = 0;
    $ERROR_COUNT   = 0;
    $INDENT        = 0;
    $RE_COLOR = qr/(?: \e \[ \d{1,2} m )/x;
}

### EXPORTED

## no critic ProhibitSubroutinePrototypes

sub info (@) {
    chomp( my @msg = @_ );
    display( INFO_PROMPT, @msg, "\n" );
}

sub success (@) {
    chomp( my @msg = @_ );
    display( [ SUCCESS_PROMPT, GREEN ], @msg, "\n" );
}

sub warning (@) {
    chomp( my @msg = @_ );
    display( [ WARNING_PROMPT, YELLOW ], @msg, "\n" );
    $WARNING_COUNT += 1;
}

sub alert (@) {
    chomp( my @msg = @_ );
    if ( @msg && $msg[0] =~ /\w/ ) {
        display( [ ALERT_PROMPT, BOLD . RED ], @msg, "\n" );
    }
    else {
        display( [ ALERT_PROMPT, BOLD . RED ],
            @msg, 'An alert has been emitted! (See previous warnings)', "\n" );
    }
    $ERROR_COUNT += 1;
    die "An alert has been raised!\n";
}

sub terminate () {
    indent_reset();
    display( TERMINATE_PROMPT,
            BOLD . 'Terminating' . RESET . ' with '
          . BOLD . $ERROR_COUNT . RESET . ' error'
          . ( $ERROR_COUNT > 1 ? 's' : '' ) . ' and '
          . BOLD . $WARNING_COUNT . RESET . ' warning'
          . ( $WARNING_COUNT > 1 ? 's' : '' ) . '. '
          . CYAN . 'Goodbye!' . "\n" );
    exit( $ERROR_COUNT > 255 ? 255 : $ERROR_COUNT );
}

sub wait_enter (;$) {
    my $msg = shift || 'Press '.BOLD.'Enter'.RESET.CYAN.' to continue';
    chomp $msg;
    display([INTERACT_PROMPT, CYAN ], $msg, ' ... ');
    <STDIN>;
    return;
}

sub interact ($@) {
    chomp( my $msg = shift );
    my $expect = join '/' => @_;
    my $answer;
  ASK: {
        display( [ INTERACT_PROMPT, CYAN ], $msg, " ($expect) " );
        chomp( $answer = <STDIN> );
        unless ( defined $answer && grep { /^$answer$/i } @_ ) {
            display(
                [ WARNING_PROMPT, YELLOW ],
                qq{Sorry, unexpected answer "$answer": expecting "},
                join( '" or "', @_ ), '" instead.', "\n"
            );
            redo ASK;
        }
    }
    return lc $answer;
}

sub process (&$;$) {
    my ( $code, $descr, $success ) = @_;
    info( INDENT_BEGIN . ucfirst($descr) . ' ... ' );
    indent_incr();
    eval { &$code };
    indent_decr();
    if ($@) {
        warning( INDENT_COMEBACK . $@ );
        exit 1;
    }
    else {
        success( INDENT_COMEBACK . ucfirst( $success || 'Success' ) . '!' );
        return 1;
    }
}

sub process_if (&$;$) {
    my ( $code, $question, $success ) = @_;
    my $ans = interact( INDENT_BEGIN . $question, qw<y n> );
    if ( $ans eq 'n' ) {
        info( INDENT_COMEBACK . 'negative answer: process not executed' );
        return 0;
    }
    else {
        indent_incr();
        eval { &$code };
        indent_decr();
        die INDENT_COMEBACK . $@ if $@;
        success( INDENT_COMEBACK . ucfirst( $success || 'Success' ) . '!' );
        return 1;
    }
}

### FOR INTERNAL USE

## use critic

sub display {
    my $prompt = shift;
    my $color  = '';
    if ( ref $prompt eq 'ARRAY' ) {
        $color  = $prompt->[1];
        $prompt = $prompt->[0];
    }
    my $first        = ucfirst shift;
    my $blank_prompt = ' ' x length( colorstrip $prompt );
    my $lines = wrap( '', "\N{U+00B7}", join( '', $first, @_ ),
        WIDTH -
          length( $blank_prompt . ( ( INDENT_GUIDE . '    ' ) x $INDENT ) ) );
    for (@$lines) {
        print STDERR $prompt . $color
          . ( ( INDENT_GUIDE . '    ' ) x $INDENT )
          . $_ . RESET;
    } continue { $prompt = $blank_prompt }
    return;
}

sub indent_reset { $INDENT = 0  }
sub indent_incr  { $INDENT += 1 }
sub indent_decr  { $INDENT -= 1; $INDENT = 0 if $INDENT < 0 }

sub wrap {
    my ( $init_tab, $subseq_tab, $str, $columns ) = @_;
    $WRAP_COLUMNS = $columns || 80;
    $WRAP_SUBSEQ_TAB =
      { TYPE => 'o', VAL => $subseq_tab, SIZE => length($subseq_tab) };
    @WRAP_STACK =
      ( { TYPE => 'o', VAL => $init_tab, SIZE => length($init_tab) } );
    @WRAP_BUFFER = ();
    $WRAP_STACKSIZE = 0;

    my ( $i, $last_valid ) = ( 0, 0 );
    $str = expand($str);

    wrap_tokenize($&) while $str =~ m/ \G (?: $RE_COLOR+ | \h+ | \n+ | . ) /xg;

    if (@WRAP_STACK) {
        my $t = '';
        for (@WRAP_STACK) { $t .= $_->{VAL} }
        push @WRAP_BUFFER, $t;
    }

    @WRAP_BUFFER = map { "$_\n" } @WRAP_BUFFER;
    chop $WRAP_BUFFER[-1]; # only want the user-supplied '\n' here
    return \@WRAP_BUFFER;
}

sub wrap_tokenize {
    my $match = shift;

    croak '$match is undef' unless defined $match;

    for ($match) {
        if (/^$RE_COLOR+$/) {
            push @WRAP_STACK, { TYPE => 'c', VAL => $_, SIZE => 0 };
        }
        elsif (/^\n+$/) {
            push @WRAP_STACK, { TYPE => 'b', VAL => $_, SIZE => 0 };
        }
        elsif (/^\h+$/) {
            push @WRAP_STACK, { TYPE => 'b', VAL => $_, SIZE => length };
            $WRAP_STACKSIZE = length;
        }
        else {
            if ( $WRAP_STACK[-1]->{TYPE} eq 'o' ) {
                $WRAP_STACK[-1]->{VAL} .= $_;
                $WRAP_STACK[-1]->{SIZE} += 1;
            }
            else { push @WRAP_STACK, { TYPE => 'o', VAL => $_, SIZE => 1 } }
            $WRAP_STACKSIZE += 1;
        }
    }

    if ( $WRAP_STACKSIZE >= $WRAP_COLUMNS ) {
        my @tmp;

        # Back to a shorter stack
        for ( $WRAP_STACK[-1]->{TYPE} ) {
            die "Internal error" unless /^[bo]$/;
            pop @WRAP_STACK if $_ eq 'b';
            unshift(@tmp, pop(@WRAP_STACK)) if $_ eq 'o';
        }

        # Clean around the newline
        while ( $WRAP_STACK[-1]->{TYPE} ne 'o' ) {
            for ( $WRAP_STACK[-1]->{TYPE} ) {
                pop @WRAP_STACK if $_ eq 'b';
                unshift( @tmp, pop(@WRAP_STACK) ) if $_ eq 'c';
            }
        }

        push @WRAP_BUFFER, reduce { $a . $b->{VAL} } ( '', @WRAP_STACK );
        @WRAP_STACK = ( $WRAP_SUBSEQ_TAB, @tmp );
        $WRAP_STACKSIZE = reduce { $a + $b->{SIZE} } ( 0, @WRAP_STACK );
    }
}


END { terminate() }

#
1;
