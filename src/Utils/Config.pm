package Utils::Config;

use 5.010_001;
use strict;
use warnings;
use utf8;
use open IO => 'utf8';
use open ':std';
use autodie;

use DateTime;
use XML::LibXML 2.00;

use lib '..';    # debug

use Carp;
use Utils::Say qw(alert);
use List::MoreUtils qw( zip );

use Objects::Allow;
use Objects::Buildrule;
use Objects::Buildsystem;
use Objects::Date;
use Objects::Defense;
use Objects::Dir;
use Objects::Document;
use Objects::Exam;
use Objects::ExamSubject;
use Objects::File;
use Objects::Grouping;
use Objects::Language;
use Objects::Link;
use Objects::Manager;
use Objects::News;
use Objects::Other;
use Objects::Path;
use Objects::Period;
use Objects::Project;
use Objects::Repository;
use Objects::Slides;
use Objects::Subject;
use Objects::Submission;
use Objects::Target;
use Objects::TopLevelDir;
use Objects::Tutorial;
use Objects::Types;
use Objects::Upload;
use Objects::VCS;

use XMLParser::RefHandler;

my %DEFAULT_PATHS;
my %DISPATCH;
my %ID;
my %IDREF;

my $REFHANDLER;
my $GLOBAL_LANGUAGE = 'en';

BEGIN {
    %DEFAULT_PATHS = (
        'Objects::ExamSubject' => 'subject',
        'Objects::Slides'      => 'slides',
        'Objects::Tutorial'    => 'tutorials',
        'Objects::Subject'     => 'subject',
    );

    $REFHANDLER = XMLParser::RefHandler->new();

    %DISPATCH = (
        project      => \&read_project,
        flavour      => \&read_textContent,
        manager      => \&read_manager,
        news         => \&read_news,
        period       => \&read_period,
        grouping     => \&read_grouping,
        language     => \&read_language,
        target       => \&read_target,
        name         => \&read_textContent,
        version      => \&read_textContent,
        command      => \&read_textContent,
        options      => \&read_textContent,
        repository   => \&read_repository,
        documents    => \&read_documents,
        tutorial     => \&read_tutorial,
        subject      => \&read_subject,
        slides       => \&read_slides,
        defense      => \&read_defense,
        other        => \&read_other,
        link         => \&read_link,
        subname      => \&read_textContent,
        date         => \&read_date,
        path         => \&read_path,
        using        => \&read_using,
        submission   => \&read_submission,
        buildsystem  => \&read_buildsystem,
        buildrule    => \&read_buildrule,
        allow        => \&read_allow,
        buildsystem  => \&read_buildsystem,
        vcs          => \&read_vcs,
        upload       => \&read_upload,
        file         => \&read_file,
        dir          => \&read_dir,
        exam         => \&read_exam,
        exam_subject => \&read_exam_subject,
    );
}

sub newXML {
    my $filename = shift or croak 'missing argument: filename';
    my $xml      = XML::LibXML->new(
        line_numbers        => 1,
        pedantic_parser     => 1,
        no_blanks           => 1,
        complete_attributes => 1,
        validation          => 1,
    )->parse_file($filename);
    $xml = $xml->documentElement;
    my $comments = $xml->findnodes('descendant-or-self::comment()');
    $comments->foreach( sub { $_->unbindNode } );
    return $xml;
}

sub readXML {
    my ($class, $filename) = @_;
    my $xml = newXML($filename);

    my @ok = qw( project exam );
    %ID = ();
    %IDREF = ();

    my $config;
    for my $type ( $xml->nodeName ) {
        alert( qq{unknown project type "$type", expected one of },
            join( '/', @ok ) )
          unless grep { $type eq $_ } @ok;

        $config = $DISPATCH{$type}->( scalar $xml );
    }
    my $index = complete_paths($config);
    bless { project => $config, paths => $index } => $class;
}

# EXPOSED FUNCTIONS ############

sub is_in ($@) { ## no critic ProhibitSubroutinePrototypes
    my @a = @_;
    my $val = shift @a;
    return scalar grep { $val eq $_ } @a;
}

sub complete_paths {
    my $config = shift;
    my %index;
    my @docs_with_path = qw(
        Objects::ExamSubject
        Objects::Other
        Objects::Slides
        Objects::Subject
        Objects::Tutorial
    );

    if ($config->isa('Objects::Project')) {
        my @docs = $config->list_documents;
        for my $d ( grep { is_in( ref $_, @docs_with_path ) } @docs ) {

            if ( ref $d eq 'Objects::Other' && ref $d->path ne 'Objects::Path' )
            {
                alert q{"path" attribute is mandatory for documents of }
                  . q{subtype "Objects::Other"};
            }

            $d->path( $d->path
                  || Objects::Path->new( verbatim => $DEFAULT_PATHS{ ref $d } ) );
            if ( exists $index{ $d->path } ) {
                alert q{can't use path value "}, $d->path, q{" for document "},
                  $d->name, q{": already used by "},
                  $index{ $d->path }->name, q{"};
            }
            $index{ $d->path } = $d;
        }
    }
    elsif ( $config->isa('Objects::Exam') ) {
        my $d = $config->subject;
        $d->path( $d->path || Objects::Path->new($DEFAULT_PATHS{ ref $d }) );
        $index{$d->path} = $d;
    }
    else { croak 'unknown project type "', ref $config, '"' }

    return \%index;
}

# read_* FUNCTIONS #############

sub read_project {
    my $node = shift;
    my $p = Objects::Project->new( name => get_attr( $node, 'name' ) );

    for my $c ( $node->childNodes ) {
        for ( $c->nodeName ) {

            if ($_ eq 'documents') {
                $p->written_in( get_attr( $c, 'written_in' ) );
                $GLOBAL_LANGUAGE = $p->written_in || $GLOBAL_LANGUAGE;
            }

            # Compute
            my $ret = $DISPATCH{$_}->($c);

            # Insert
            if    ( $_ eq 'manager' )    { $p->add_managers($ret) }
            elsif ( $_ eq 'news' )       { $p->news($ret) }
            elsif ( $_ eq 'period' )     { $p->period($ret) }
            elsif ( $_ eq 'grouping' )   { $p->grouping($ret) }
            elsif ( $_ eq 'language' )   { $p->add_languages($ret) }
            elsif ( $_ eq 'target' )     { $p->add_targets($ret) }
            elsif ( $_ eq 'repository' ) { $p->add_repositories($ret) }
            elsif ( $_ eq 'documents' )  { $p->add_documents(@$ret) }
            else { croak(qq{unknown parameter "$_" for Objects::Project}) }
        }
    }

    $p->default_written_in($GLOBAL_LANGUAGE);

    return $p;
}

sub read_manager {
    my $node = shift;
    my ( $firstname, $lastname, $login );

    my $children = $node->childNodes;

    # Firstname
    for ( $children->get_node(1) ) {
        check_node_type( $_, 'firstname' );
        $firstname = $_->textContent;
    }

    # Lastname
    for ( $children->get_node(2) ) {
        check_node_type( $_, 'lastname' );
        $lastname = $_->textContent;
    }

    # Login
    for ( $children->get_node(3) ) {
        check_node_type( $_, 'login' );
        $login = $_->textContent;
    }

    my $manager = Objects::Manager->new(
        firstname => $firstname,
        lastname  => $lastname,
        login     => $login,
    );

    return $manager;
}

sub read_news {
    my $node = shift;
    my ( $newsgroup, $tag );

    my $children = $node->childNodes;

    # Newsgroup
    for ( $children->get_node(1) ) {
        check_node_type( $_, 'newsgroup' );
        $newsgroup = $_->textContent;
    }

    # Tag
    for ( $children->get_node(2) ) {
        check_node_type( $_, 'tag' );
        $tag = $_->textContent;
    }

    my $news = Objects::News->new(
        newsgroup => $newsgroup,
        tag       => $tag,
    );

    return $news;
}

sub read_period {
    my ( $node, $period ) = @_;

    my ( $begin, $end ) = map { $DISPATCH{date}->($_) } $node->childNodes;

    alert( "begin: expected Objects::Date, got ", ref $begin )
      unless $begin->isa('Objects::Date');
    alert( "end: expected Objects::Date, got ", ref $end )
      unless $end->isa('Objects::Date');
    alert("end < begin") if DateTime->compare( $begin->dt, $end->dt ) > 0;

    if ( defined $period && $period->does('Role::Limited') ) {
        $period->begin($begin);
        $period->end($end);
    }
    else {
        $period = Objects::Period->new( begin => $begin, end => $end );
    }

    return $period;
}

sub read_grouping {
    my $node = shift;

    my $g = Objects::Grouping->new();

    $g->max_groups(get_attr( $node, 'max_groups' )) if get_attr( $node, 'max_groups' );
    $g->max_members(get_attr( $node, 'max_members' )) if get_attr( $node, 'max_members' );

    if (defined $node->firstChild and $node->firstChild->nodeName eq 'period') {
        $g->period($DISPATCH{period}->($node->firstChild, $g));
    }

    return $g;
}

sub read_language {
    my $node = shift;
    my $lang = Objects::Language->new(
        name => $node->textContent,
    );
    $REFHANDLER->offer( get_attr( $node, 'lang_id' ) => $lang );
    $lang->add_targets( %{ extract_refs( $node, 'targeting' ) } );
    return $lang;
}

sub read_target {
    my $node   = shift;
    my ($name, $version, $command, $options);

    for my $c ( $node->childNodes ) {
        $name    = $c->textContent if $c->nodeName eq 'name';
        $version = $c->textContent if $c->nodeName eq 'version';
        $command = $c->textContent if $c->nodeName eq 'command';
        $options = $c->textContent if $c->nodeName eq 'options';
    }

    my $target = Objects::Target->new(
        name => $name,
        kind => get_attr( $node, 'type' ),
    );

    $target->version( $version ) if defined $version;
    $target->command( $command ) if defined $command;
    $target->options( $options ) if defined $options;

    $REFHANDLER->offer( get_attr( $node, 'target_id' ) => $target );

    return $target;
}

sub read_repository {
    my $node = shift;

    my $repository = Objects::Repository->new(
        kind => get_attr( $node, 'type' ),
        quota => get_attr( $node, 'quota'),
        url => $node->textContent,
    );

    $REFHANDLER->offer( get_attr($node, 'repo_id') => $repository );

    return $repository;
}

sub read_documents {
    my $node = shift;
    my $docs = [];

    for my $c ($node->childNodes) {
        push @$docs, $DISPATCH{$c->nodeName}->($c);
    }

    return $docs;
}

sub read_tutorial {
    my $node = shift;
    my $tut = Objects::Tutorial->new( name => get_attr( $node, 'name' ) );

    $tut->written_in( get_attr( $node, 'written_in' ) || $GLOBAL_LANGUAGE );

    for my $c ( $node->childNodes ) {
        for ( $c->nodeName ) {
            my $ret = $DISPATCH{$_}->($c);

            $tut->subname($ret)        if $_ eq 'subname';
            $tut->period($ret)         if $_ eq 'period';
            $tut->path($ret)           if $_ eq 'path';
            $tut->add_languages(@$ret) if $_ eq 'using';
        }
    }

    return $tut;
}

sub read_subject {
    my $node = shift;
    my $subject = Objects::Subject->new( name => get_attr( $node, 'name' ) );

    $subject->written_in( get_attr( $node, 'written_in' ) || $GLOBAL_LANGUAGE );

    for my $c ( $node->childNodes ) {
        for ( $c->nodeName ) {
            my $ret = $DISPATCH{$_}->($c);

            $subject->subname($ret)         if $_ eq 'subname';
            $subject->period($ret)          if $_ eq 'period';
            $subject->path($ret)            if $_ eq 'path';
            $subject->add_languages(@$ret)  if $_ eq 'using';
            $subject->add_submissions($ret) if $_ eq 'submission';
        }
    }

    return $subject;
}

sub read_slides {
    my $node = shift;
    my $slides = Objects::Slides->new( name => get_attr( $node, 'name' ) );

    $slides->written_in( get_attr( $node, 'written_in' ) || $GLOBAL_LANGUAGE );

    for my $c ( $node->childNodes ) {
        for ( $c->nodeName ) {
            my $ret = $DISPATCH{$_}->($c);

            $slides->subname($ret) if $_ eq 'subname';
            $slides->date($ret)    if $_ eq 'date';
            $slides->path($ret)    if $_ eq 'path';
        }
    }

    return $slides;
}

sub read_defense {
    my $node = shift;
    my ($date, $file);

    for my $c ($node->childNodes) {
        for ( $c->nodeName ) {
            my $ret = $DISPATCH{$_}->($c);

            $date = $ret if $_ eq 'date';
            $file = $ret if $_ eq 'file';
        }
    }

    my $defense = Objects::Defense->new(
        name => get_attr( $node, 'name' ),
        date => $date,
        file => $file,
    );

    return $defense;
}

sub read_other {
    my $node = shift;
    my $other = Objects::Other->new( name => get_attr( $node, 'name' ) );

    $other->written_in( get_attr( $node, 'written_in' ) || $GLOBAL_LANGUAGE );

    for my $c ( $node->childNodes ) {
        for ( $c->nodeName ) {
            my $ret = $DISPATCH{$_}->($c);

            $other->subname($ret) if $_ eq 'subname';
            $other->date($ret)    if $_ eq 'date';
            $other->path($ret)    if $_ eq 'path';
        }
    }

    return $other;
}

sub read_link {
    my $node = shift;
    my $link =
      Objects::Link->new(
        name => get_attr( $node, 'name' ),
        url => get_attr( $node, 'url' ),
    );

    $link->descr( get_attr( $node, 'description' ) )
      if get_attr( $node, 'description' );

    if ( $node->hasChildNodes ) {
        check_node_type( $node->firstChild, 'period' );
        $link->period( $DISPATCH{period}->( $node->firstChild ) );
    }

    return $link;
}

sub read_textContent {
    my $node = shift;
    return $node->textContent;
}

sub read_date {
    my $node = shift;
    return Objects::Date->new( $node->textContent );
}

sub read_path {
    my $node = shift;
    return Objects::Path->new( verbatim => $node->textContent );
}

sub read_using {
    my $node = shift;
    return [ values %{ extract_refs( $node, 'languages' ) } ];
}

sub read_submission {
    my $node = shift;

    my ( $name, $period, @allow, $buildsystem, $method );

    for my $c ( $node->childNodes ) {
        for ( $c->nodeName ) {
            my $ret = $DISPATCH{$_}->($c);

            $name   = $ret if $_ eq 'name';
            $period = $ret if $_ eq 'period';
            push @allow, $ret if $_ eq 'allow';
            $buildsystem = $ret if $_ eq 'buildsystem';
            $method = $ret if $_ eq 'vcs' || $_ eq 'upload';
        }
    }

    my $subm = Objects::Submission->new(
        name   => $name,
        kind   => get_attr( $node, 'type' ),
        method => $method,
        period => $period,
    );

    $subm->add_allow(@allow) if @allow;
    $subm->buildsystem($buildsystem) if defined $buildsystem;

    return $subm;
}

sub read_buildsystem {
    my $node = shift;
    my $bs = Objects::Buildsystem->new(
        command => get_attr( $node, 'command' ),
    );
    $bs->add_rules( map { $DISPATCH{$_->nodeName}->($_) } $node->childNodes );
    return $bs;
}

sub read_buildrule {
    my $node = shift;
    my $br = Objects::Buildrule->new( name => $node->textContent );
    $br->descr( get_attr( $node, 'descr' ) ) if get_attr( $node, 'descr' );
    return $br;
}

sub read_allow {
    my $node = shift;
    return Objects::Allow->new(
        content  => $node->textContent,
        kind     => get_attr( $node, 'type' ),
        manpage  => get_attr( $node, 'manpage' ),
        url      => get_attr( $node, 'url' ),
        from_lib => get_attr( $node, 'from_lib' ),
    );
}

sub read_upload {
    my $node = shift;
    my $upload = Objects::Upload->new( url => get_attr( $node, 'url' ) );

    $upload->format( get_attr( $node, 'format' ) ) if get_attr( $node, 'format' );
    $upload->quota( get_attr( $node, 'quota' ) ) if get_attr( $node, 'quota' );

    my $tld = new Objects::TopLevelDir;
    for my $c ( $node->childNodes ) {
        $tld->add( $DISPATCH{ $c->nodeName }->($c) );
    }
    $upload->tree($tld);

    return $upload;
}

sub read_vcs {
    my $node   = shift;
    my $vcs = new Objects::VCS(
        repo_id => get_attr( $node, 'repo_id' ),
        tag     => get_attr( $node, 'tag' ),
    );

    $vcs->token( get_attr( $node, 'token' ) );

    my $tld = new Objects::TopLevelDir;
    for my $c ($node->childNodes) {
        $tld->add( $DISPATCH{$c->nodeName}->($c) );
    }
    $vcs->tree( $tld );

    return $vcs;
}

sub read_file {
    my $node = shift;
    my $file = Objects::File->new(
        name => get_attr( $node, 'name' ),
        descr => get_attr( $node, 'description' ),
        mandatory => get_attr( $node, 'mandatory' ),
    );

    if ($node->hasChildNodes) {
        check_node_type($node->firstChild, 'period');
        $file->period( $DISPATCH{period}->($node->firstChild) );
    }

    return $file;
}

sub read_dir {
    my $node = shift;
    my $dir = Objects::Dir->new(
        name => get_attr( $node, 'name'),
        descr => get_attr($node, 'description'),
        mandatory => get_attr($node, 'mandatory')
    );

    for my $c ( $node->childNodes ) {
        if ( $c->nodeName eq 'file' ) {
            $dir->tree( $DISPATCH{file}->($c) );
        }
        elsif ( $c->nodeName eq 'dir' ) {
            $dir->tree( $DISPATCH{dir}->($c) );
        }
        else {
            alert( 'internal error: a Dir can contain only Files or Dirs; "',
                $c->nodeName, '" found' );
        }
    }

    return $dir;
}

sub read_exam {
    my $node = shift;
    my $exam = Objects::Exam->new( name => get_attr( $node, 'name' ) );

    for my $n ( $node->childNodes ) {
        for ( $n->nodeName ) {

            # Compute
            my $ret = $DISPATCH{$_}->($n);

            # Insert
            if    ( $_ eq 'manager' )      { $exam->add_managers($ret) }
            elsif ( $_ eq 'period' )       { $exam->period($ret) }
            elsif ( $_ eq 'language' )     { $exam->add_languages($ret) }
            elsif ( $_ eq 'target' )       { $exam->add_targets($ret) }
            elsif ( $_ eq 'exam_subject' ) { $exam->subject($ret) }
            else { croak(qq{unknown parameter "$_" for Objects::Exam}) }
        }
    }

    return $exam;
}

sub read_exam_subject {
    my $node = shift;
    my $es   = Objects::ExamSubject->new( name => get_attr( $node, 'name' ) );

    $es->written_in( get_attr( $node, 'written_in' ) || $GLOBAL_LANGUAGE );

    for my $c ( $node->childNodes ) {
        for ($c->nodeName) {
            my $ret = $DISPATCH{$_}->($c);

            if ($_ eq 'subname') { $es->subname($ret) }
            elsif ($_ eq 'path') { $es->path($ret) }
            elsif ($_ eq 'using') { $es->add_languages(@$ret) }
            else { croak(qq{unknown parameter "$_" for Objects::ExamSubject}) }
        }
    }

    return $es;
}

# UTILITIES ####################

sub get_attr {
    my $attrs = $_[0]->attributes->{NodeMap}
      or alert("internal error: can't access NodeMap");
    return '' unless exists $attrs->{ $_[1] };
    return $attrs->{ $_[1] }->value;
}

sub extract_refs {
    my ( $node, $attribute ) = @_;
    my @ids  = split / / => get_attr( $node, $attribute );
    my @refs = $REFHANDLER->ask_for( @ids );
    return { zip(@ids, @refs) };
}

sub check_node_type {
    my ( $node, $expected ) = @_;
    my $given = $node->nodeName;
    alert(qq{invalid node type "$given", expected "$expected"})
      if $given ne $expected;
    return 1;
}

1;
