package Make::DocInfo;

use 5.010_001;
use strict;
use warnings;

use Moose;
use Moose::Util::TypeConstraints;

use Objects::Types;
use Objects::Project;
use Objects::Exam;
use Objects::Document;
use Objects::Path;

has project => (
    is       => 'ro',
    isa      => 'Objects::Project | Objects::Exam',
    required => 1,
);

has document => (
    is       => 'ro',
    isa      => 'Objects::Document',
    required => 1,
);

has written_in => (
    is      => 'rw',
    isa     => 'Butler::SpokenLanguage',
    default => 'en',
);

has team => (
    is       => 'ro',
    isa      => enum( [qw( acu yaka )] ),
    required => 1,
);

has imagedir => (
    is       => 'ro',
    isa      => 'Objects::Path',
    required => 1,
);

has version => (
    is      => 'ro',
    isa     => 'Butler::PositiveInt',
    default => 1,
);

has month => (
    is => 'ro',
    isa => 'Butler::PositiveInt',
    default => DateTime->today->month(),
);

has year => (
    is => 'ro',
    isa => 'Butler::PositiveInt',
    default => DateTime->today->year(),
);

no Moose;
__PACKAGE__->meta->make_immutable;
1;
