package Make::Templates;

use 5.010_001;
use strict;
use warnings;
use utf8;
use open IO => 'utf8';
use open ':std';

use constant TEMPLATE_TYPE => 'acu';    # yaka | acu

use lib '..';                           # debug

use Carp 'croak';
use Encode;
use POSIX 'strftime';
use File::Spec;
use List::Util 'reduce';
use Text::Template 1.46;

use Utils::Say qw(alert);
use Utils::Git;
use Make::DocInfo;
use Objects::Path;

# doctype    := regular | beamer
# docsubtype := tutorial | subject | slides | other | examsubject
# lang       := fr | en

my ( $a, $b );    # for List::Util::reduce

my $ROOT_DIR;
my $TEMPLATE_DIR;

my %PARTS;
my %PROJECT_INFOS;

BEGIN {
    $ROOT_DIR = File::Spec->rootdir();
    $TEMPLATE_DIR =
      File::Spec->catdir( $ROOT_DIR, 'sgoinfre', 'butler', 'templates',
        TEMPLATE_TYPE );

    %PARTS = (
        tutorial => [qw( std frontpage copyright_and_toc newpage MAIN motto )],
        subject  => [
            qw( std frontpage copyright_and_toc newpage obligations
              advice newpage submissions directorytree newpage MAIN motto )
        ],
        other  => [qw( std frontpage copyright_and_toc newpage MAIN motto )],
        slides => [qw( std frontpage copyright_and_toc MAIN )],
        examsubject => [ qw( std frontpage copyright_and_toc newpage
            obligations submissions newpage MAIN motto ) ],
    );
}

sub new {
    my ( $class, $project, $docname ) = @_;

    croak 'new() is a class method' unless ref $class eq '';
    $project->isa('Objects::Project') || $project->isa('Objects::Exam')
      or croak 'expected "Objects::Project" or "Objects::Exam", got "',
      ref($project), '"';

    my $doc;
    if ( $project->isa('Objects::Project') ) {
        $doc = $project->get_document($docname)
          or alert(qq{no known document named "$docname"});
    }
    elsif ( $project->isa('Objects::Exam') ) {
        $doc = $project->subject;
    }
    else { croak 'unknown project type "', ref($project), '"' }

    my ($doctype, $docsubtype);
    $doctype = $doc->isa('Objects::Slides') ? 'beamer' : 'regular';
    $docsubtype = do { my ($a) = (ref $doc) =~ /^Objects::(.+)$/; lc $a };

    my $git_tag = do {
        my $default_path = Objects::Path->standard_for($docsubtype);
        $doc->path->get_absolute ne $default_path->get_absolute
          ? Utils::Git::last_tag(
            doctype => $docsubtype,
            path    => $doc->path->get_absolute
          )
          : Utils::Git::last_tag( doctype => $docsubtype );
    };

    my $info = Make::DocInfo->new(
        project => $project,
        document => $doc,
        team => TEMPLATE_TYPE,
        imagedir => Objects::Path->new(
            File::Spec->catdir($TEMPLATE_DIR, 'images')
        ),
        written_in => $doc->written_in || $project->written_in,
        version => $git_tag->{version},
    );

    my $self =
      bless { doctype => $doctype, lang => $info->written_in } => $class;

    $self->{template} = [
        map {
            $_ eq 'MAIN' ? '__MAIN__' :
            decode_utf8(_name2template( $_, $info->written_in, $doctype )
              ->fill_in( HASH => { DOC => \$info } ))
        } @{ $PARTS{$docsubtype} }
    ];

    return $self;
}

sub assemble {
    my ($self, $mainfile) = @_;
    croak 'assemble() is an instance method' if ref $self eq '';
    my $out = '';
    for my $tmpl (@{$self->{template}}) {
        if ($tmpl eq '__MAIN__') {
            open my $fh, '<', $mainfile or alert "can't open $mainfile: $!";
            local $/;
            $out .= <$fh>;
        }
        else { $out .= $tmpl }
    }
    return $out;
}

sub template_dir {
    my ( $class, $dir ) = @_;
    croak 'template_dir() is a class method' unless ref $class eq '';
    $TEMPLATE_DIR = $dir if defined $dir;
    $TEMPLATE_DIR;
}

sub pandoc_template {
    my ( $class, $doctype ) = @_;
    croak 'pandoc_template() is a class method' unless ref $class eq '';
    return File::Spec->catfile( $TEMPLATE_DIR, $doctype, 'template.tex' );
}

########################################################################

sub _escape_for_latex {
    my $str = shift;
    return unless defined $str;
    $str =~ s/ ([\\_@&#]) /\\$1/xg;
    return $str;
}

sub _fill {
    my ( $self, $values ) = @_;
    $self->isa('Make::Template') or croak 'fill() is an instance method';
    my $result = reduce { $a . "\n" . $b->fill_in( HASH => $values ) }
    ( '', @{ $self->template } );
    alert($Text::Template::ERROR) unless defined $result;
    return $result;
}

sub _name2template {
    my ( $name, $lang, $doctype ) = @_;

    croak 'TEMPLATE_DIR unspecified' unless defined $TEMPLATE_DIR;

    my $filename =
      File::Spec->catfile( $TEMPLATE_DIR, $doctype, $lang, $name . '.tex' );

    my $tmpl = do {
        open my $fh, '<', $filename or alert "can't open $filename: $!";
        local $/;
        <$fh>
    };

    return Text::Template->new(
        TYPE => 'STRING',
        SOURCE => $tmpl,
        DELIMITERS => [ '[@--', '--@]' ],
        PREPEND => <<'EOF',
use DateTime;

use Make::DocInfo;
use Data::Dumper;

use Objects::Allow;
use Objects::Buildrule;
use Objects::Buildsystem;
use Objects::Date;
use Objects::Defense;
use Objects::Dir;
use Objects::Document;
use Objects::Exam;
use Objects::ExamSubject;
use Objects::File;
use Objects::Grouping;
use Objects::Language;
use Objects::Link;
use Objects::Manager;
use Objects::News;
use Objects::Other;
use Objects::Path;
use Objects::Period;
use Objects::Project;
use Objects::Repository;
use Objects::Slides;
use Objects::Subject;
use Objects::Submission;
use Objects::Target;
use Objects::TopLevelDir;
use Objects::Tutorial;
use Objects::Types;
use Objects::Upload;
use Objects::VCS;

no warnings 'redefine';
sub esc {
    my $str = shift;
    return unless defined $str;
    $str =~ s/ ([\_@&#}{]) /\\$1/xg;
    return $str;
}
EOF
    )
      || alert $Text::Template::ERROR;
}

1;
