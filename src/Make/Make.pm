package Make::Make;

use 5.010_001;
use strict;
use warnings;
use utf8;
use open IO => 'utf8';
use open ':std';

use lib '..';    # debug

use Carp;
use Text::Template;
use File::Spec;

use Utils::Say qw( alert );
use Utils::Run qw( can_run run );
use Make::Templates;

my ( $PANDOC_REGULAR_TEMPLATE, $PANDOC_BEAMER_TEMPLATE );
my ( %ENGINE,                  %PARAMS );

BEGIN {
    $PANDOC_REGULAR_TEMPLATE = Make::Templates->pandoc_template('regular');
    $PANDOC_BEAMER_TEMPLATE  = Make::Templates->pandoc_template('beamer');

    %ENGINE = (
        markdown => 'pandoc',
        latex    => 'xelatex',
    );
    %PARAMS = (
        pandoc =>
          "--toc --latex-engine=$ENGINE{latex} --slide-level=3 --listings",
        xelatex => '-halt-on-error',
    );

    for ( keys %ENGINE ) {
        can_run($_) or alert("$_ is not installed");
    }
}

# args:
#   doc    => document to build: Objects::Document or LaTeX filename
#   config => Utils::Config object to use
#   target => 'pdf' | 'latex'
sub make {
    my %arg = @_;

    $arg{doc}->isa('Objects::Document')
      or croak 'expected Objects::Document, got ', ref $arg{doc};
    $arg{config}->isa('Utils::Config')
      or croak 'expected Utils::Config, got ', ref $arg{config};

    if ($arg{target} eq 'latex') {
    }
    elsif ($arg{target} eq 'pdf') {
    }
    else { alert qq[unknown target "$arg{target}"] }
}

###
### CONVERTERS ###########
###

sub markdown2latex {
    my ( $infile, $outfile ) = @_;

    check_file_extension( $infile,  'md' );
    check_file_extension( $outfile, 'tex' );

    process {
        run(
            command => $ENGINE{markdown}
              . " $PARAMS{$ENGINE{markdown}}"
              . ' -f markdown -t latex'
              . " -o $outfile $infile",
            timeout => 30
        );
    } "compiling $infile (Markdown) to $outfile (regular LaTeX)";

    return 1;
}

sub markdown2beamertex {
    my ( $infile, $outfile ) = @_;

    check_file_extension( $infile,  'md' );
    check_file_extension( $outfile, 'tex' );

    process {
        run(
            command => $ENGINE{markdown}
              . " $PARAMS{$ENGINE{markdown}}"
              . ' -f markdown -t beamer'
              . " -o $outfile $infile",
            timeout => 30
        );
    } "compiling $infile (Markdown) to $outfile (LaTeX Beamer)";

    return 1;
}

sub markdown2beamerpdf {
    my ( $infile, $outfile ) = @_;

    check_file_extension( $infile,  'md' );
    check_file_extension( $outfile, 'pdf' );

    my $cmd =
        "$ENGINE{markdown} $PARAMS{$ENGINE{markdown}} -f markdown -t beamer"
      . " --template=$PANDOC_BEAMER_TEMPLATE -o $outfile $infile";

    process {
        run(
            command => join( ' && ', $cmd x 2 ),
            timeout => 90
        );
    } "compiling $infile (Markdown) to $outfile (PDF Beamer)";

    return 1;
}

sub markdown2pdf {
    my ( $infile, $outfile ) = @_;

    check_file_extension( $infile,  'md' );
    check_file_extension( $outfile, 'pdf' );

    my $cmd =
        "$ENGINE{markdown} $PARAMS{$ENGINE{markdown}} -f markdown -t latex"
      . " --template=$PANDOC_REGULAR_TEMPLATE -o $outfile $infile";

    process {
        run(
            command => join( ' && ', $cmd x 2 ),
            timeout => 90
        );
    } "compiling $infile (Markdown) to $outfile (regular PDF)";
}

sub latex2pdf {
    my ( $infile, $outfile ) = @_;

    check_file_extension( $infile,  'tex' );
    check_file_extension( $outfile, 'pdf' );

    my $cmd =
        "$ENGINE{latex} $PARAMS{$ENGINE{latex}}"
      . " -jobname $outfile $infile";

    process {
        run(
            command => join( ' && ', $cmd x 2 ),
            timeout => 90
        );
    } "compiling $infile (Markdown) to $outfile (regular PDF)";
}

sub beamertex2beamer {
    ...;
}

###
### UTILITIES ############
###

sub external_call_failed {
    'External call failed: ' . $_[0] . ( @_ > 1 ? " ($_[1])" : '' ) . "\n";
}

sub check_file_extension {
    my ( $filename, $expected ) = @_;

    if ( $filename !~ /\./ ) {
        $expected eq ''
          or croak qq{$filename has no extension (expected: "$expected")};
    }

    my $eq =
      ref $expected eq 'Regexp'
      ? sub { $_[0] =~ $_[1] }
      : sub { $_[0] eq $_[1] };

    $filename =~ m{ \. (.+) $ }x;
    $eq->( $1, $expected )
      or croak qq{$filename has extension "$1" (expected: "$expected")};
}

1;
