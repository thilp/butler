SRCDIR    = src
TESTDIR   = t
PROVER    = prove -f
COVER     = cover -test -report html_basic

all: cover

test:
	${PROVER} ${TESTDIR}

testv:
	${PROVER} -v ${TESTDIR}

cover:
	${COVER}
