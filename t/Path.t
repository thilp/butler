use 5.010_001;
use strict;
use warnings;

use Cwd;
use File::Spec;
use File::Temp;

my $TMPDIR;
my %FHS;

BEGIN {
    $TMPDIR = File::Spec->tmpdir();
    %FHS    = (
        "$TMPDIR/FILE" => File::Temp->new(),
        '$CWD/FILE'    => File::Temp->new( DIR => Cwd::cwd() ),
        "$TMPDIR/DIR"  => File::Temp->newdir(),
        '$CWD/DIR'     => File::Temp->newdir( DIR => Cwd::cwd() ),
    );
}

use lib 'src';

use Test::More tests => 2 + keys(%FHS);
use Test::Exception;
use Test::Differences;

BEGIN {
    use_ok('Objects::Path') or BAIL_OUT('target module not loaded');
}

can_ok(
    'Objects::Path',
    qw(
      new
      get_base
      set_base
      get_canonical
      get_absolute
      get_relative
      ensure
      )
);

while ( my ( $k, $v ) = each %FHS ) {

    my $file = $v->isa('File::Temp::Dir') ? $v->dirname : $v->filename;
    $file = File::Spec->abs2rel($file) if $file =~ m|^$TMPDIR|;

    subtest "Testing with $k" => sub {
        plan tests => 6;

        {
            my $p = new_ok( 'Objects::Path' => [ verbatim => $file ] )
              or diag 'the only required argument for the constructor '
              . 'should be "verbatim"';
        }

        {
            my $p = new_ok( 'Objects::Path' => [$file] )
              or diag "with only one arg, the constructor should expect "
              . "that it is in fact 'verbatim => \$arg'";

            is( $p->get_base, Cwd::cwd(),
                '"base" defaults to Cwd::cwd()’s output' );

            is(
                $p->get_relative,
                File::Spec->abs2rel($file),
                "get_relative works"
            );

            ok( $p->ensure( sub { -e } ), "path checks for existence" );
            is(
                $p->ensure( sub { -d } ),
                $v->isa('File::Temp::Dir'),
                "path checks for directory-ness"
            );
        }

    };
}
