use 5.010_001;
use strict;
use warnings;

use lib 'src';

use Test::More tests => 16;
use Test::Exception;
use Test::Differences;

BEGIN {
    use_ok('XMLParser::RefHandler') or BAIL_OUT('target module not loaded');
}

can_ok(
    'XMLParser::RefHandler',
    qw(
      new
      ask_for
      offer
      )
);

my @names = ( "\a2ð" x 20, qw( totoro 42 ) );

# One ref at a time
{

    my $rh = new_ok('XMLParser::RefHandler');
    my @refs;

    dies_ok { $refs[0] = XMLParser::RefHandler->ask_for( $names[0] ) }
    'ask_for dies when called on class';

    subtest 'ask_for: 1 ref, scalar context' => sub {
        $rh = XMLParser::RefHandler->new();
        lives_ok { $refs[0] = $rh->ask_for( $names[0] ) }
        'ask_for does not die';
        is( $refs[0]->mref_value, $names[0], 'ask_for works' );
    };

    subtest 'ask_for: 3 refs, scalar context' => sub {
        $rh = XMLParser::RefHandler->new();
        lives_ok { $refs[0] = $rh->ask_for(@names) } 'ask_for does not die';
        is( $refs[0], scalar @names, 'ask_for works' );
    };

    subtest 'ask_for: 1 ref, list context' => sub {
        $rh = XMLParser::RefHandler->new();
        lives_ok { @refs = $rh->ask_for( $names[0] ) } 'ask_for does not die';
        eq_or_diff(
            [ map { $_->mref_value } @refs ],
            [ $names[0] ],
            'ask_for works'
        );
    };

    subtest 'ask_for: 3 refs, list context' => sub {
        $rh = XMLParser::RefHandler->new();
        lives_ok { @refs = $rh->ask_for(@names) } 'ask_for does not die';
        eq_or_diff( [ map { $_->mref_value } @refs ], \@names,
            'ask_for works' );
    };
}

{
    my $rh = XMLParser::RefHandler->new();
    my $ref1  = $rh->ask_for( $names[0] );
    my @refs2 = $rh->ask_for(@names);
    is( scalar( grep { $_ eq $ref1 } @refs2 ),
        1, 'ask_for resolve already known IDs' );
}


package A; ## no critic
sub new { bless { toto => $_[1] } => $_[0] }
sub get { $_[0]->{toto} }

package main;

use List::MoreUtils qw( zip );

SKIP: {
    my $rh      = XMLParser::RefHandler->new();
    my @ids     = qw( a b c );
    my @objects = map { A->new( uc $_ ) } @ids;

    dies_ok { my $a = XMLParser::RefHandler->offer( 42 => 'b' ) }
    'offer dies when called on class';

    my @refs = $rh->ask_for(@ids);
    lives_ok {
        $rh->offer( zip @ids, @objects );
    }
    "offer doesn't die on trivial cases"
      or skip "because continue testing offer() would be useless", 2;

    eq_or_diff(
        [ map { $_->get } @refs ],
        [ map { $_->get } @objects ],
        'resolved references behave the same as objects'
    );
    eq_or_diff( [ map { $_->mref_value } @refs ],
        \@objects, 'resolved references ARE the objects' );
}

SKIP: {
    my $rh      = XMLParser::RefHandler->new();
    my @ids     = qw( a b );
    my @objects = map { A->new( uc $_ ) } (@ids, 'c');

    my @refs = $rh->ask_for(@ids);

    lives_ok {
        $rh->offer( zip( @ids, @{[ @objects[ 0 .. 1 ] ]} ), c => $objects[2] );
    } 'offer doesn\'t die with unknown IDs';

    eq_or_diff(
        [ map { $_->get } @refs ],
        [ map { $_->get } @objects[0..1] ],
        'refs with known IDs are resolved'
    );

    my $lastref = $rh->ask_for('c');

    is( $lastref->mref_value, $objects[2], 'late ref is well resolved' );
}

done_testing();
