package A::Green::Cow;

use 5.010_001;
use strict;
use warnings;

sub new { bless { say => 'meuh!' } }
sub tellme { $_[0]->{say} }

package main;

use 5.010_001;
use strict;
use warnings;

use lib 'src';

use Test::More tests => 10;
use Test::Exception;

BEGIN {
    use_ok('XMLParser::MagicRef')
      or BAIL_OUT('target module not loaded');
}

can_ok(
    'XMLParser::MagicRef',
    qw(
      mref_new
      mref_value
      mref_resolved
      mref_resolve
      )
);

my $value    = 'vache';
my $newvalue = A::Green::Cow->new();
my $ref      = XMLParser::MagicRef->mref_new($value);

isa_ok( $ref, 'XMLParser::MagicRef' );

is( $ref->mref_value, $value, 'after creation, first value is set' );

is( $ref->mref_resolved, 0, 'after creation, ref is not marked resolved' );

throws_ok { $ref->method } qr/not yet resolved/,
  'before resolution, method calls are catched';

$ref->mref_resolve($newvalue);

is( $ref->mref_resolved, 1, 'after resolution, ref is marked resolved' );

is( $ref->mref_value, $newvalue, 'after resolution, new value is set' );

lives_and { is( $ref->tellme, $newvalue->tellme ) }
'after resolution, method calls are resolved';

throws_ok { $ref->mref_resolve($newvalue) } qr/already resolved/,
  'after resolution, can\'t resolve() again';
